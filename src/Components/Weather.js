import axios from 'axios';
import moment from 'moment';
import React, { useEffect, useState } from 'react';
import { APIS } from '../constants';
import WeatherDetails from './WeatherDetails';

const Weather = () => {

  const [weatherData, setWeatherData] = useState({});
  const [loading, setLoading] = useState(true);
  const [isFaranheat, setisFaranheat] = useState(false);
  const [error, setError] = useState("");

  useEffect(() => {
    try {
      async function fetcData() {
        const response = await axios.get(APIS.weatherApi);
        if (response.data) {
          setWeatherData(response.data);
          setLoading(false);
        }
      }

      fetcData();
    } catch (err) {
      setLoading(false);
      setError(err)
    }
  }, []);

  const { country, name } = !loading && weatherData?.location;
  const { temp_c, temp_f, condition, humidity, wind_kph } = !loading && weatherData?.current;
  const { hour } = !loading && weatherData.forecast.forecastday[0];

  return <div className='container'>
    <h4>{condition?.text}</h4>
    <span>{`${country}, ${name}`}</span>
    <button onClick={() => setisFaranheat(false)} className='btn-primary m-2'>c°</button>
    <button onClick={() => setisFaranheat(true)} className='btn-danger m-2'>f°</button>
    <div className='row border p-2 m-2'>
      {hour && hour.length > 0 && hour.map((data) => {
        return (<WeatherDetails key={data?.time} data={data} isFaranheatSelected={isFaranheat} />
        )
      })}
    </div>
  </div>;
};


export default Weather;
