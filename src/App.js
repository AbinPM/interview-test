import './App.css';
import Weather from './Components/Weather';
import 'bootstrap/dist/css/bootstrap.min.css';

function App() {
  return (
    <div className="App">
      <h1>Wheather App</h1>
      <Weather />
    </div>
  );
}

export default App;
